<?php
echo '<pre>';
//Constante no PHP
define('QTD_PAGINAS', 10);

echo "Valor da minha constante é: " . QTD_PAGINAS . "\n";

//Variavel para passar valor para uma constante

$ip_do_banco = '192.168.45.12';

define('IP_DO_BANCO', $ip_do_banco);

echo 'O iP do SGDB é: ' . IP_DO_BANCO;
//constantes magica
echo "\n Estou na linha: " .__LINE__;
echo "\n Estou na linha: " .__LINE__;
echo "\n Este é o arquivo : " .__FILE__;

//Muito bom para depurar o código
echo"\n\n";
var_dump($ip_do_banco);

/*
* Agora fica mais legal
* É hora de vetores
* Array
*/
echo "\n\n";
$dias_da_semana = ['dom','ter','qua','qui','sex','sab','dom'];

var_dump($dias_da_semana);

unset($dias_da_semana);//destroi a variavel

$dias_da_semana = array(0 => 'dom',
                        1 => 'seg',
                        2 => 'ter',
                        3 => 'qua',
                        4 => 'qui',
                        5 => 'sex',
                        6 => 'sab');
echo "\n\n";

var_dump($dias_da_semana);

/*
 * Teste de git
 * */



echo '</pre>';
